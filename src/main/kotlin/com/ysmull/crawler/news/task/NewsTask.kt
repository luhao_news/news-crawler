package com.ysmull.crawler.news.task

import com.ysmull.crawler.news.dao.NewsDao
import com.ysmull.crawler.news.model.po.Img
import com.ysmull.crawler.news.model.po.NewsPO
import com.ysmull.crawler.news.service.getNewsBy
import com.ysmull.crawler.news.util.execute
import org.jsoup.Jsoup
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.lang.Thread.sleep


enum class NewsType(var desc: String, var code: String, var order: Int) {
    YULE("娱乐", "yule", 1),
    TIYU("体育", "tiyu", 2),
    JIANKANG("健康", "jiankang", 3),
    SHEHUI("社会", "shehui", 4),
    //    REDIAN("热点", "redian"), // 问题
    CAIJING("财经", "caijing", 5),
    KEJI("科技", "keji", 6),
    GUOJI("国际", "guoji", 7),
    //    RENWEN("人文", "renwen"),
    QICHE("汽车", "qiche", 8),
    QINGGAN("情感", "qinggan", 9),
    XINGZUO("星座", "xingzuo", 10),
    NBA("NBA", "nba", 11),
    SHISHANG("时尚", "shishang", 12),
    YOUXI("游戏", "youxi", 13)
}

@Component
class NewsTask {

    @Autowired
    private lateinit var newsDao: NewsDao

    private val logger = LoggerFactory.getLogger(javaClass)

    fun task(type: NewsType) {
        while (true) {
            for (page in 1..8) {
                try {
                    var newsPOList = getNewsBy(type.code, page, (page - 1) * 12).data.filter {
                        it.isvideo == "0"
                    }.map {
                        val doc = Jsoup.connect(it.url).validateTLSCertificates(false).get()
                        val article = doc.select("article")
                        NewsPO(
                            type = type.code,
                            title = it.topic,
                            description = it.desc,
                            content = article.toString(),
                            pics = it.miniimg.map { miniimg -> Img(miniimg) },
                            source = it.source
                        )
                    }
                    val existNewsTitles = newsDao.getByTypeAndTitle(type.code, newsPOList.map { n -> n.title }).map { n -> n.title }
                    newsPOList = newsPOList.filter { news ->
                        !existNewsTitles.contains(news.title)
                    }
                    logger.info("$type: ${newsPOList.size}")
                    newsDao.batchInsert(newsPOList)
                    sleep((1000L * 60..1000L * 90).random())
                } catch (e: Exception) {
                    logger.error(e.message)
                }
            }
        }
    }

    fun doTask() {
        newsDao.insertNewsType(NewsType.values())
        NewsType.values().forEach {
            execute {
                task(it)
            }
        }
    }
}