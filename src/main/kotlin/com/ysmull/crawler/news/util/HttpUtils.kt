package com.ysmull.crawler.news.util

import com.ysmull.crawler.news.https.TrustAllCerts
import okhttp3.*
import okio.Buffer
import org.slf4j.LoggerFactory
import java.io.IOException
import java.security.SecureRandom
import java.util.concurrent.TimeUnit
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager


object HttpUtils {

    private val logger = LoggerFactory.getLogger(javaClass)

    private val client: OkHttpClient by lazy {
        val pool = ConnectionPool(5, 10000, TimeUnit.MILLISECONDS)
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.connectionPool(pool);
        clientBuilder.sslSocketFactory(createSSLSocketFactory(), TrustAllCerts())
        clientBuilder.connectTimeout(10, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(10, TimeUnit.SECONDS)
        clientBuilder.readTimeout(20, TimeUnit.SECONDS)
        clientBuilder.hostnameVerifier { hostname, session -> true }
        clientBuilder.addInterceptor(LoggingInterceptor())
        clientBuilder.build()
    }

    internal class LoggingInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val request = chain.request()
            val buffer = Buffer()
            val body = request.body()
            body?.writeTo(buffer)
            val t1 = System.nanoTime()
            val response = chain.proceed(request)
            val t2 = System.nanoTime()
//            logger.info(
//                String.format(
//                    "[%s] %s in %.1fms -- %s",
//                    request.method(), response.request().url(), (t2 - t1) / 1e6, buffer.readUtf8()
//                )
//            )
            return response
        }
    }

    private fun buildRequest(builder: Request.Builder, cookie: String? = null, ssl: Boolean): Request {
        builder.addHeader("Host", "refreshnews.toutiaokuaibao.com")
            .addHeader("User-Agent", "okhttp/3.4.1")
        if (cookie != null) {
            builder.addHeader("cookie", cookie)
        }
        if (!ssl) {
            builder.addHeader("accept-encoding", "gzip, deflate, br")
        }
        return builder.build()
    }

    fun postJSON(url: String, params: Any): String? {
        val mediaType = MediaType.parse("application/json")
        val body = RequestBody.create(mediaType, params.toJson())
        val builder = Request.Builder().url(url).post(body)
        val request = buildRequest(builder = builder, ssl = url.startsWith("https"))
        client.newCall(request).execute().use { response ->
            return response.body()?.string()
        }
    }

    private fun convertMap(params: Map<String, Any?>): String {
        return params.map { "${it.key}=${it.value}" }.joinToString(separator = "&")
    }

    fun postURLEncoded(url: String, params: Map<String, Any?>): String {
        val mediaType = MediaType.parse("application/x-www-form-urlencoded")
        val body = RequestBody.create(mediaType, convertMap(params))
        val builder = Request.Builder().url(url).post(body)
        val request = buildRequest(builder = builder, ssl = url.startsWith("https"))
        client.newCall(request).execute().use { response ->
            return response.body()!!.string()
        }
    }

    private fun createSSLSocketFactory(): SSLSocketFactory {
        val sc = SSLContext.getInstance("TLS")
        sc.init(null, arrayOf<TrustManager>(TrustAllCerts()), SecureRandom())
        return sc.socketFactory
    }

}
