package com.ysmull.crawler.news.util

import com.github.javafaker.Faker
import java.util.*
import java.util.UUID.randomUUID
import kotlin.random.Random
import org.apache.commons.lang3.RandomUtils.nextBytes
import kotlin.experimental.and


val faker = Faker(Locale("zh-cn"))

fun fakeIME(): String {
    return "${(100000000000000..999999999999999).random()}"
}

fun key(): String = randomUUID().toString().replace("-", "").take(16)

fun city(): String = faker.address().city().replace("市$".toRegex(), "")

fun province(): String = faker.address().state().replace("省$".toRegex(), "")

fun appqid(): String =
    mutableListOf("xiaomi", "huawei", "vivo", "oppo", "oneplus").shuffled().last() + "${(180323..190323).random()}"

fun os(): String = "Android${(7..9).random()}.0.${(0..1).random()}"

fun appVersion(): Pair<String, String> {
    val r = (1..5).random()
    return Pair("2.2.$r", "02020$r")
}

private fun randomMACAddress(): String {
    val rand = Random
    val macAddr = ByteArray(6)
    rand.nextBytes(macAddr)
    macAddr[0] = (macAddr[0] and 254.toByte())
    val sb = StringBuilder(18)
    for (b in macAddr) {
        if (sb.isNotEmpty())
            sb.append(":")
        sb.append(String.format("%02x", b))
    }
    return sb.toString()
}

fun generatePageQuery(type: String, pageNum: Int, idx: Int): Map<String, Any?> {
    val (ver, appver) = appVersion()
    return mapOf(
        "type" to type,
        "startkey" to "||${Random.nextInt()}||||",
        "newkey" to "|||${Random.nextInt()}||||",
        "pgnum" to pageNum,
        "idx" to idx,
        "key" to key(), //"16325bf19c84dcfa" , 4a7cfbeba69b7b33
        "softtype" to "TouTiaoKuaiBao",
        "softname" to "TTKBAndroid",
        "ime" to fakeIME(),
        "appqid" to appqid(),
        "apptypeid" to "TTKB",
        "ver" to ver,
        "os" to os(),
        "ttaccid" to "null",
        "appver" to appver,
        "deviceid" to key(),
        "position" to province(),
        "iswifi" to "wifi",
        "channellabel" to null,
        "citypos" to null,
        "sublocal" to null,
        "hispos" to "${province()},${city()}",
        "ispack_s" to 1,
        "appinfo" to """{"mac":"${randomMACAddress()}","ssid":"${key()}","bssid":"${randomMACAddress()}","lat":"null","lng":"null","ele":"${(11..99).random()}","state":"1","temperature":"${(0..33).random()}"}""",
        "sclog" to 1
    )
}