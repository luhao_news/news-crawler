package com.ysmull.crawler.news.util

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.springframework.scheduling.concurrent.CustomizableThreadFactory
import java.util.concurrent.Executors


var gson = Gson()

fun <T> T.toJson(): String = gson.toJson(this)

inline fun <reified T> String.fromJson(): T = gson.fromJson(this, object : TypeToken<T>() {}.type)


fun Any.print(postfix: String = "") = println(this.toString() + postfix)


internal val executor = Executors.newCachedThreadPool(CustomizableThreadFactory("execute-"))

fun execute(process: () -> Unit) {
    executor.execute(process)
}