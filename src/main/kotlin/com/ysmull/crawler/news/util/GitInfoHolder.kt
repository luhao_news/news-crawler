package com.ysmull.crawler.news.util


import com.ysmull.crawler.annotation.NoArg

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component


@NoArg
@Component
@ConfigurationProperties(prefix = "git")
@PropertySource("classpath:git.properties")
data class GitInfoHolder(
    var branch: String,
    var commit: Map<String, String>
) {
    override fun toString(): String {
        return ("branch: " + branch + "<br>"
            + "user: " + commit["user.name"] + "<br>"
            + "commit: " + commit["id"] + "<br>"
            + "message: " + commit["message.full"])
    }
}
