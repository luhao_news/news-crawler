package com.ysmull.crawler.news.service

import com.ysmull.crawler.news.model.dto.Page
import com.ysmull.crawler.news.util.HttpUtils
import com.ysmull.crawler.news.util.fromJson
import com.ysmull.crawler.news.util.generatePageQuery

fun getNewsBy(type: String, page: Int, idx: Int = 1): Page {
    val res = HttpUtils.postURLEncoded(
        "https://refreshnews2.dftoutiao.com/TtNews/newsgzip",
        generatePageQuery(type, page, idx)
    )
    return res.fromJson()
}
