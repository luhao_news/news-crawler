package com.ysmull.crawler.news.model.dto

data class MiniImg(
    val imgheight: Int,
    val imgwidth: Int,
    val imgname: String,
    val src: String
)

data class Item(
    val date: String,
    val desc: String,
    val topic: String,
    val url: String,
    val miniimg: List<MiniImg>,
    val type: String,
    val urlfrom: String,
    val urlpv: String,
    val source: String,
    val isvideo: String
)

data class Page(val data: List<Item>)