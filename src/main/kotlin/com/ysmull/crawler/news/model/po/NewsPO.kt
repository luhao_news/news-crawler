package com.ysmull.crawler.news.model.po

import com.ysmull.crawler.annotation.NoArg
import com.ysmull.crawler.news.model.dto.MiniImg


data class Img(
    val height: Int,
    val width: Int,
    val name: String,
    val src: String
) {
    constructor(img: MiniImg) : this(img.imgheight, img.imgwidth, img.imgname, img.src)
}

@NoArg
data class NewsPO(
    val id: Long? = null,
    val type: String,
    val title: String,
    val description: String,
    val content: String,
    val pics: List<Img>?,
    val source: String?
)