package com.ysmull.crawler.news.dao

import com.ysmull.crawler.news.model.po.Img
import com.ysmull.crawler.news.model.po.NewsPO
import com.ysmull.crawler.news.task.NewsType
import com.ysmull.crawler.news.util.execute
import com.ysmull.crawler.news.util.fromJson
import com.ysmull.crawler.news.util.toJson
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository
import java.sql.ResultSet


fun <T> createParameterSource(receiver: T, block: MutableMap<String, Any?>.(T) -> Unit): MapSqlParameterSource {
    val source = MapSqlParameterSource()
    val map = mutableMapOf<String, Any?>()
    map.block(receiver)
    return source.addValues(map)
}

fun <T> createBatchParameterSource(
    receiverList: List<T>,
    block: MutableMap<String, Any?>.(T) -> Unit
): Array<MapSqlParameterSource> {
    return receiverList.map { item ->
        createParameterSource(item, block)
    }.toTypedArray()
}

@Repository
class NewsDao {
    @Autowired
    private lateinit var namedParameterJdbcTemplate: NamedParameterJdbcTemplate

    private val logger = LoggerFactory.getLogger(javaClass)

    val rowMapper = { rs: ResultSet, _: Int ->
        NewsPO(
            id = rs.getLong("id"),
            type = rs.getString("type"),
            title = rs.getString("title"),
            description = rs.getString("description"),
            content = rs.getString("content"),
            pics = rs.getString("pics").fromJson<List<Img>>(),
            source = rs.getString("source")
        )
    }

    fun insert(news: NewsPO) {
        val sql =
            """INSERT INTO news(`type`, `title`, `description`, `content`, `pics`) VALUES(:type, :title, :description, :content, :pics)"""
        val parameterSource = createParameterSource(news) {
            this["type"] = it.type
            this["title"] = it.title
            this["description"] = it.description
            this["content"] = it.content
            this["pics"] = it.pics.toJson()
        }
        namedParameterJdbcTemplate.update(sql, parameterSource)
    }

    fun batchInsert(newsList: List<NewsPO>?) {
        if (newsList == null) return;
        val sql =
            """INSERT INTO news(`type`, `title`, `description`, `content`, `pics`, `source`) VALUES(:type, :title, :description, :content, :pics, :source)"""
        val batch = createBatchParameterSource(newsList) {
            this["type"] = it.type
            this["title"] = it.title
            this["description"] = it.description
            this["content"] = it.content
            this["pics"] = it.pics.toJson()
            this["source"] = it.source
        }
        execute {
            namedParameterJdbcTemplate.batchUpdate(sql, batch)
        }
    }

    fun getByTypeAndTitle(type: String, titles: List<String>): List<NewsPO> {
        if (titles.isEmpty()) {
            return emptyList()
        }
        val sql = "select * from news where type = '$type' and title in (:titles)"
        return namedParameterJdbcTemplate.query(sql, mapOf("titles" to titles), rowMapper)
    }

    fun insertNewsType(types: Array<NewsType>) {
        val sql =
            """insert into news_type (`code`, `name`, `order`) values (:code, :desc, :order)
                on duplicate key update `order` = values(`order`) and `name` = values(`name`)"""
        val batch = createBatchParameterSource(types.toList()) {
            this["code"] = it.code
            this["desc"] = it.desc
            this["order"] = it.order
        }
        execute {
            namedParameterJdbcTemplate.batchUpdate(sql, batch)
        }
    }
}