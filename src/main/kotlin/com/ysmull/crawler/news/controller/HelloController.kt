package com.ysmull.crawler.news.controller

import com.ysmull.crawler.news.util.GitInfoHolder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import javax.annotation.Resource

@RestController
@RequestMapping("/")
class HelloController {

    @Autowired
    private lateinit var gitInfoHolder: GitInfoHolder

    @GetMapping("/version")
    @ResponseBody
    internal fun version(): String {
        return gitInfoHolder.toString()
    }

}
