package com.ysmull.crawler.news

import com.ysmull.crawler.news.task.NewsTask
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.runApplication
import org.springframework.context.event.EventListener
import java.util.concurrent.Executors


@SpringBootApplication
class NewsApplication(val newsTask: NewsTask) {
    @EventListener(ApplicationReadyEvent::class)
    fun doSomethingAfterStartup() {
        println("==>启动新闻爬取任务")
        executor.execute {
            try {
                newsTask.doTask()
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }
}


private val executor = Executors.newSingleThreadExecutor()
fun main(args: Array<String>) {
    runApplication<NewsApplication>(*args)
}

