create table if not exists ads
(
    id          int auto_increment
        primary key,
    type        varchar(50)                         not null comment '广告类型',
    link        text                                not null comment '客户链接',
    name        text                                not null,
    publish     tinyint   default 0                 not null,
    create_time timestamp default CURRENT_TIMESTAMP not null,
    modify_time timestamp                           null,
    weight      double    default 0                 null,
    `order`     int       default 0                 null,
    img_link    text                                null,
    times       int      default 0                 null
);

create table if not exists backend_user
(
    id        int auto_increment
        primary key,
    user_name varchar(50)   not null,
    password  varchar(255)  not null,
    constraint backend_user_user_name_uindex
        unique (user_name)
)
    charset = utf8mb4;



create table if not exists news
(
    id          int auto_increment
        primary key,
    type        varchar(20) default ''                not null comment '新闻分类',
    title       text                                  null,
    description text                                  null,
    content     mediumtext                            null,
    pics        json                                  null,
    source      varchar(255)                          null,
    create_time timestamp   default CURRENT_TIMESTAMP null
)
    collate = utf8mb4_unicode_ci;

create table if not exists news_type
(
    id      int(11) unsigned auto_increment
        primary key,
    code    varchar(255) null,
    name    varchar(255) null,
    `order` int(3)       null,
    constraint news_type_pk
        unique (code)
)
    charset = utf8mb4;

create table if not exists version
(
    id      int(11) unsigned auto_increment
        primary key,
    version varchar(11)  null,
    url     varchar(255) null
)
    charset = utf8mb4;

