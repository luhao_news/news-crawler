#!/bin/bash
mvn clean package -Dmaven.test.skip
cid=$(docker build . -t="news-crawler" | grep "Successfully built" | awk '{print $3}')
docker tag $cid swr.cn-north-4.myhuaweicloud.com/luhao_lqr/news-crawler
docker push swr.cn-north-4.myhuaweicloud.com/luhao_lqr/news-crawler
ssh hw1 "sh /root/deploy/news_crawler.sh";

