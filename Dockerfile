FROM hub.c.163.com/maoyusu/serverjre:latest
MAINTAINER ysmull@aliyun.com

COPY ./target/news-crawler-0.0.1-SNAPSHOT.jar /news/

WORKDIR /news

EXPOSE 8888

CMD ["/usr/bin/java", "-Duser.timezone=Asia/Shanghai","-Dfile.encoding=UTF-8", "-XX:+UseG1GC", "-Xms256m", "-Xmx256m", "-jar", "news-crawler-0.0.1-SNAPSHOT.jar", "--spring.profiles.active=prod"]